#!/bin/bash

usage() {
  echo -e "\n"
  echo 'Usage:'
  echo "$0 --dir; help"
}
TAS=true;

while [[ $1 = -?* ]]; do
  case $1 in
    -h|--help)
        usage >&2; exit 0
        ;;
    -d|--dir)
        shift; DIR="${1}"
        ;;
    -v|--var)
    	shift; g1="${1}"
    	;;


    *) echo "invalid option: '$1'."; usage; exit 1; ;;
  esac
  shift
done
args+=("$@")

# Check exists and type of file
  if [[ ! -d ${DIR} ]]
  then
    DIR=$(pwd)
  fi
  if [[ ! -v ${n1} ]]
  then
    n1=0
  fi

 cd $DIR
 while (${TAS})
 do
	if [ $g1 == "1" ]; then
	ls
 	elif [ $g1 == "2" ]; then
 	ls -l
  	elif [ $g1 == "3" ]; then
 	ls -d
 	elif [ $g1 == "4" ]; then
 	ls -C
 	elif [ $g1 == "5" ]; then
 	ls -x
 	elif [ $g1 == "6" ]; then
 	 ls -a
 	else
 	  for first in "$DIR"/*
 	 do
 	 echo "$first"
  	done
 	fi

PS3="Выберите операцию: "

select opt in  find change_directory copy delete quit; do

  case $opt in
    find)
	 read -p "Variant display? " g1
	  if [ $g1 == "1" ]; then
	ls
 	elif [ $g1 == "2" ]; then
 	cd $DIR
 	ls -l
  	elif [ $g1 == "3" ]; then
 	ls -d
 	elif [ $g1 == "4" ]; then
 	ls -C
 	elif [ $g1 == "5" ]; then
 	ls -x
 	 elif [ $g1 == "6" ]; then
 	 ls -a
 	else
 	 for first in "$DIR"/*
 	 do
 	 echo "$first"
  	done
 	fi
	break
      ;;
    change_directory)
    read -p "What directory we change? " n1
      DIR=$n1
      cd $DIR
      break
      ;;
    copy)
      read -p "What file we are copy? " n1
      read -p "Name a copy? " n2
      FIRST=$n1
      SECOND=$n2
      	cp -r "$FIRST" "$SECOND"
      	break
      ;;
    delete)
      read -p "What file we are delete?  " n1
 	find ${DIR} \( -iname "$n1" \) -exec rm -r {} \;
 	break
      ;;
    quit)
      exit 0
      ;;
    *) 
      echo "Недопустимая опция $REPLY"
      ;;
  esac
done
done

