#!/bin/bash

usage() {
  echo -e "\n"
  echo 'Usage:'
  echo "$0 --dir; help"
}

while [[ $1 = -?* ]]; do
  case $1 in
    -h|--help)
        usage >&2; exit 0
        ;;
    -d|--dir)
        shift; DIR="${1}"
        ;;
    *) echo "invalid option: '$1'."; usage; exit 1; ;;
  esac
  shift
done
  if [[ ! -d ${DIR} ]]
  then
    DIR=$(pwd)
  fi

all_files=$(find $DIR -mtime 30 )
for solo_files in $all_files; do
	mv $solo_files "~$solo_files"
	find ${DIR} \( -iname "*.tmp"  -o -iname "-*" -o -iname "_*" -o -iname "~*" \) -exec rm -r {} \;
done


