#include <linux/init.h>
#include <linux/module.h>
#include <linux/spi/spi.h>
#include <linux/delay.h>
#include <linux/gpio.h>
#include <linux/device.h>
#include "ili9341.h"
#include "fonts.c"

MODULE_LICENSE("GPL");
MODULE_VERSION("1.0");
MODULE_AUTHOR("Maksym.Lipchanskyi <maxl@meta.ua>");
MODULE_DESCRIPTION("LCD Demo");
#define PICT_WIDTH1 (30)
#define PICT_HEIGHT1 (30)
static char pict_mask1[PICT_WIDTH1][PICT_HEIGHT1] = {
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
	{1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},};
	static u16 pict_2[PICT_WIDTH1][PICT_HEIGHT1], pict_3[PICT_WIDTH1][PICT_HEIGHT1];
	static int pict_x1 = 5, pict_y1 = 50;
	static int dir_x1 = 4, dir_y1 = 4;
	
	
	#define PICT_WIDTH (7)
	#define PICT_HEIGHT (7)
	static char pict_mask[PICT_WIDTH][PICT_HEIGHT] = {
		{0, 0, 0, 1, 0, 0, 0},
		{0, 0, 1, 1, 1, 0, 0},
		{0, 1, 1, 1, 1, 1, 0},
		{1, 1, 1, 1, 1, 1, 1},
		{0, 1, 1, 1, 1, 1, 0},
		{0, 0, 1, 1, 1, 0, 0},
		{0, 0, 0, 1, 0, 0, 0},};
		static u16 pict_0[PICT_WIDTH][PICT_HEIGHT], pict_1[PICT_WIDTH][PICT_HEIGHT];
		static int pict_x = 100, pict_y = 50;
		static int dir_x = 4, dir_y = 4;
		#define DATA_SIZE	90

		static u16 frame_buffer[LCD_WIDTH * LCD_HEIGHT];
		static u16 buffer[LCD_WIDTH * LCD_HEIGHT];
		static struct spi_device *lcd_spi_device;

		static void lcd_reset(void)
		{
			gpio_set_value(LCD_PIN_RESET, 0);
			mdelay(5);
			gpio_set_value(LCD_PIN_RESET, 1);
		}

		static void lcd_write_command(u8 cmd)
		{
			gpio_set_value(LCD_PIN_DC, 0);
			spi_write(lcd_spi_device, &cmd, sizeof(cmd));
		}

		static void lcd_write_data(u8 *buff, size_t buff_size)
		{
			size_t i = 0;
			
			gpio_set_value(LCD_PIN_DC, 1);
			while (buff_size > DATA_SIZE) {
				spi_write(lcd_spi_device, buff + i, DATA_SIZE);
				i += DATA_SIZE;
				buff_size -= DATA_SIZE;
			}
			spi_write(lcd_spi_device, buff + i, buff_size);
		}

		static void lcd_set_address_window(u16 x0, u16 y0, u16 x1, u16 y1)
		{

			lcd_write_command(LCD_CASET);
			{
				uint8_t data[] = { (x0 >> 8) & 0xFF, x0 & 0xFF,
				(x1 >> 8) & 0xFF, x1 & 0xFF };
				lcd_write_data(data, sizeof(data));
			}

			lcd_write_command(LCD_RASET);
			{
				uint8_t data[] = { (y0 >> 8) & 0xFF, y0 & 0xFF,
				(y1 >> 8) & 0xFF, y1 & 0xFF };
				lcd_write_data(data, sizeof(data));
			}

			lcd_write_command(LCD_RAMWR);
		}

		inline void lcd_update_screen(void)
		{
			lcd_write_data((u8*)frame_buffer, sizeof(u16) * LCD_WIDTH * LCD_HEIGHT);
		}

		void lcd_draw_pixel(u16 x, u16 y, u16 color)
		{
			if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
				return;
			}
			
			frame_buffer[x + LCD_WIDTH * y] = (color >> 8) | (color << 8);
			lcd_update_screen();
		}

		void lcd_fill_rectangle(u16 x, u16 y, u16 w, u16 h, u16 color)
		{
			u16 i;
			u16 j;

			if ((x >= LCD_WIDTH) || (y >= LCD_HEIGHT)) {
				return;
			}

			if ((x + w - 1) > LCD_WIDTH) {
				w = LCD_WIDTH - x;
			}

			if ((y + h - 1) > LCD_HEIGHT) {
				h = LCD_HEIGHT - y;
			}

			for (j = 0; j < h; j++) {
				for (i = 0; i < w; i++) {
					frame_buffer[(x + LCD_WIDTH * y) + (i + LCD_WIDTH * j)] = (color >> 8) | (color << 8);
				}
			}
			lcd_update_screen();
		}

		void lcd_fill_screen(u16 color)
		{
			lcd_fill_rectangle(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1, color);
		}

		static void lcd_put_char(u16 x, u16 y, char ch, FontDef font, u16 color, u16 bgcolor)
		{
			u32 i, b, j;

			for (i = 0; i < font.height; i++) {
				b = font.data[(ch - 32) * font.height + i];
				for (j = 0; j < font.width; j++) {
					if ((b << j) & 0x8000)  {
						frame_buffer[(x + LCD_WIDTH * y) + (j + LCD_WIDTH * i)] =
						(color >> 8) | (color << 8);
					}
					else {
						frame_buffer[(x + LCD_WIDTH * y) + (j + LCD_WIDTH * i)] =
						(bgcolor >> 8) | (bgcolor << 8);
					}
				}
			}
		}

		void lcd_put_str(u16 x, u16 y, const char* str, FontDef font, u16 color, u16 bgcolor)
		{
			while (*str) {
				if (x + font.width >= LCD_WIDTH) {
					x = 0;
					y += font.height;
					if (y + font.height >= LCD_HEIGHT) {
						break;
					}

					if (*str == ' ') {
						// skip spaces in the beginning of the new line
						str++;
						continue;
					}
				}
				lcd_put_char(x, y, *str, font, color, bgcolor);
				x += font.width;
				str++;
			}
		}

		void lcd_init_ili9341(void)
		{
			// SOFTWARE RESET
			lcd_write_command(0x01);
			mdelay(1000);

			// POWER CONTROL A
			lcd_write_command(0xCB);
			{
				u8 data[] = { 0x39, 0x2C, 0x00, 0x34, 0x02 };
				lcd_write_data(data, sizeof(data));
			}

			// POWER CONTROL B
			lcd_write_command(0xCF);
			{
				u8 data[] = { 0x00, 0xC1, 0x30 };
				lcd_write_data(data, sizeof(data));
			}

			// DRIVER TIMING CONTROL A
			lcd_write_command(0xE8);
			{
				u8 data[] = { 0x85, 0x00, 0x78 };
				lcd_write_data(data, sizeof(data));
			}

			// DRIVER TIMING CONTROL B
			lcd_write_command(0xEA);
			{
				u8 data[] = { 0x00, 0x00 };
				lcd_write_data(data, sizeof(data));
			}

			// POWER ON SEQUENCE CONTROL
			lcd_write_command(0xED);
			{
				u8 data[] = { 0x64, 0x03, 0x12, 0x81 };
				lcd_write_data(data, sizeof(data));
			}

			// PUMP RATIO CONTROL
			lcd_write_command(0xF7);
			{
				u8 data[] = { 0x20 };
				lcd_write_data(data, sizeof(data));
			}

			// POWER CONTROL,VRH[5:0]
			lcd_write_command(0xC0);
			{
				u8 data[] = { 0x23 };
				lcd_write_data(data, sizeof(data));
			}

			// POWER CONTROL,SAP[2:0];BT[3:0]
			lcd_write_command(0xC1);
			{
				u8 data[] = { 0x10 };
				lcd_write_data(data, sizeof(data));
			}

			// VCM CONTROL
			lcd_write_command(0xC5);
			{
				u8 data[] = { 0x3E, 0x28 };
				lcd_write_data(data, sizeof(data));
			}

			// VCM CONTROL 2
			lcd_write_command(0xC7);
			{
				u8 data[] = { 0x86 };
				lcd_write_data(data, sizeof(data));
			}

			// PIXEL FORMAT
			lcd_write_command(0x3A);
			{
				u8 data[] = { 0x55 };
				lcd_write_data(data, sizeof(data));
			}
			
			// FRAME RATIO CONTROL, STANDARD RGB COLOR
			lcd_write_command(0xB1);
			{
				u8 data[] = { 0x00, 0x18 };
				lcd_write_data(data, sizeof(data));
			}
			
			// DISPLAY FUNCTION CONTROL
			lcd_write_command(0xB6);
			{
				u8 data[] = { 0x08, 0x82, 0x27 };
				lcd_write_data(data, sizeof(data));
			}

			// 3GAMMA FUNCTION DISABLE
			lcd_write_command(0xF2);
			{
				u8 data[] = { 0x00 };
				lcd_write_data(data, sizeof(data));
			}

			// GAMMA CURVE SELECTED
			lcd_write_command(0x26);
			{
				u8 data[] = { 0x01 };
				lcd_write_data(data, sizeof(data));
			}
			
			// POSITIVE GAMMA CORRECTION
			lcd_write_command(0xE0);
			{
				u8 data[] = { 0x0F, 0x31, 0x2B, 0x0C, 0x0E, 0x08, 0x4E, 0xF1,
				0x37, 0x07, 0x10, 0x03, 0x0E, 0x09, 0x00 };
				lcd_write_data(data, sizeof(data));
			}
			
			// NEGATIVE GAMMA CORRECTION
			lcd_write_command(0xE1);
			{
				u8 data[] = { 0x00, 0x0E, 0x14, 0x03, 0x11, 0x07, 0x31, 0xC1,
				0x48, 0x08, 0x0F, 0x0C, 0x31, 0x36, 0x0F };
				lcd_write_data(data, sizeof(data));
			}

			// EXIT SLEEP
			lcd_write_command(0x11);
			mdelay(120);
			
			// TURN ON DISPLAY
			lcd_write_command(0x29);

			// MEMORY ACCESS CONTROL
			lcd_write_command(0x36);
			{
				u8 data[] = { 0x28 };
				lcd_write_data(data, sizeof(data));
			}

			// INVERSION
			//	lcd_write_command(0x21);
		}

		static void __exit mod_exit(void)
		{

			gpio_free(LCD_PIN_DC);
			//	gpio_free(LCD_PIN_RESET);

			if (lcd_spi_device) {
				spi_unregister_device(lcd_spi_device);
			}

		}

		static int __init mod_init(void)
		{
			int ret;
			struct spi_master *master;

			struct spi_board_info lcd_info = {
				.modalias = "LCD",
				.max_speed_hz = 25e6,
				.bus_num = 0,
				.chip_select = 0,
				.mode = SPI_MODE_0,
			};

			master = spi_busnum_to_master(lcd_info.bus_num);
			if (!master) {
				printk("MASTER not found.\n");
				ret = -ENODEV;
				goto out;
			}

			lcd_spi_device = spi_new_device(master, &lcd_info);
			if (!lcd_spi_device) {
				printk("FAILED to create slave.\n");
				ret = -ENODEV;
				goto out;
			}

			lcd_spi_device->bits_per_word = 8,

			ret = spi_setup(lcd_spi_device);
			if (ret) {
				printk("FAILED to setup slave.\n");
				spi_unregister_device(lcd_spi_device);
				ret = -ENODEV;
				goto out;
			}


			gpio_request(LCD_PIN_RESET, "LCD_PIN_RESET");
			gpio_direction_output(LCD_PIN_RESET, 0);
			gpio_request(LCD_PIN_DC, "LCD_PIN_DC");
			gpio_direction_output(LCD_PIN_DC, 0);
			lcd_reset();

			lcd_init_ili9341();
			int t1000, x500, x50, x55, t900, t950, enter1;
			t1000 = 1;
			x500 = 1;
			memset(frame_buffer, COLOR_BLACK, sizeof(frame_buffer));
			lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
			lcd_put_str(50, 0, "Choose level", Font_11x18, COLOR_BLUE, COLOR_BLACK);
			lcd_put_str(70, 20, "1", Font_11x18, COLOR_BLUE, COLOR_BLACK);
			lcd_put_str(90, 20, "2", Font_11x18, COLOR_BLUE, COLOR_BLACK);
			lcd_put_str(110, 20, "3", Font_11x18, COLOR_BLUE, COLOR_BLACK);
			lcd_put_str(130, 20, "4", Font_11x18, COLOR_BLUE, COLOR_BLACK);
			lcd_update_screen();
			do {
				x50 = gpio_get_value(24);
				if (t900 - x50 == 1){
					if (x500<5){
						x500 ++;
					}
				}
				x55 = gpio_get_value(18);
				if (t950 - x55 == 1){
					if (x500>0){
						x500 --;
					}
				}
				if (x500 == 1){
					lcd_put_str(70, 20, "1", Font_11x18, COLOR_BLUE, COLOR_WHITE);
					lcd_put_str(90, 20, "2", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					lcd_put_str(110, 20, "3", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					lcd_put_str(130, 20, "4", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					dir_x = 1, dir_y = 1;
				}
				if (x500 == 2){
					lcd_put_str(70, 20, "1", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					lcd_put_str(90, 20, "2", Font_11x18, COLOR_BLUE, COLOR_WHITE);
					lcd_put_str(110, 20, "3", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					lcd_put_str(130, 20, "4", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					dir_x = 2, dir_y = 2;
				}
				if (x500 == 3){
					lcd_put_str(70, 20, "1", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					lcd_put_str(90, 20, "2", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					lcd_put_str(110, 20, "3", Font_11x18, COLOR_BLUE, COLOR_WHITE);
					lcd_put_str(130, 20, "4", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					dir_x = 3, dir_y = 3;
				}
				if (x500 == 4){
					lcd_put_str(70, 20, "1", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					lcd_put_str(90, 20, "2", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					lcd_put_str(110, 20, "3", Font_11x18, COLOR_BLUE, COLOR_BLACK);
					lcd_put_str(130, 20, "4", Font_11x18, COLOR_BLUE, COLOR_WHITE);
					dir_x = 4, dir_y = 4;
				}
				t900 = x50;
				t950 = x55;
				lcd_update_screen();
				enter1 = gpio_get_value(23);
				if (enter1 == 0){
					break;
				}
			}while(t1000 == 1);
			lcd_put_str(50, 0, "Choose level", Font_11x18, COLOR_BLACK, COLOR_BLACK);
			lcd_put_str(70, 20, "1", Font_11x18, COLOR_BLACK, COLOR_BLACK);
			lcd_put_str(90, 20, "2", Font_11x18, COLOR_BLACK, COLOR_BLACK);
			lcd_put_str(110, 20, "3", Font_11x18, COLOR_BLACK, COLOR_BLACK);
			lcd_put_str(130, 20, "4", Font_11x18, COLOR_BLACK, COLOR_BLACK);
			lcd_update_screen();
			u16 i2;
			u16 j2;
			u16 x1 = 0;
			u16 y1 = 0;
			u16 w1 = 220;
			u16 h1 = 320;
			u16 color1 = COLOR_COLOR565(0, 0, 0);

			if ((x1 >= LCD_WIDTH) || (y1 >= LCD_HEIGHT)) {
				return 1;
			}

			if ((x1 + w1 - 1) > LCD_WIDTH) {
				w1 = LCD_WIDTH - x1;
			}

			if ((y1 + h1 - 1) > LCD_HEIGHT) {
				h1 = LCD_HEIGHT - y1;
			}

			for (j2 = 0; j2 < h1; j2++) {
				for (i2 = 0; i2 < w1; i2++) {
					buffer[(x1 + LCD_WIDTH * y1) + (i2 + LCD_WIDTH * j2)] = (color1 >> 8) | (color1 << 8);
				}
			}

			lcd_write_data((u8*)buffer, sizeof(u16) * LCD_WIDTH * LCD_HEIGHT);
			int x20, x15, x10, x5, y5, x, y, t, t1, t2, t3, x30, x35, x40, x45;
			t = 0;
			t3 = 1;
			t1 = 1;
			char x21[5];
			x20 = 0;
			for (x5 = 0; x5 < PICT_WIDTH1; x5++) {
				for (y5 = 0; y5 < PICT_HEIGHT1; y5++) {
					pict_2[y5][x5] = frame_buffer[pict_x1 + x5 + (LCD_WIDTH * (pict_y1 + y5))];
				}
			}
			for (x = 0; x < PICT_WIDTH; x++) {
				for (y = 0; y < PICT_HEIGHT; y++) {
					pict_0[y][x] = frame_buffer[pict_x + x + (LCD_WIDTH * (pict_y + y))];
				}
			}
			pict_x = 50, pict_y = 50;
			pict_x1 = 10, pict_y1 = 50;
			do {
				sprintf(x21, "%d", x20);
				x45 = gpio_get_value(23);
				lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
				lcd_put_str(10, 20, "Press middle button 1 time for continue game", Font_7x10, COLOR_BLUE, COLOR_BLACK);
				lcd_put_str(0, 60, "Press middle button for 1second for exit game", Font_7x10, COLOR_BLUE, COLOR_BLACK);
				lcd_put_str(60, 120, "SCORE: ", Font_16x26, COLOR_BLUE, COLOR_BLACK);
				lcd_put_str(170, 120,  x21, Font_16x26, COLOR_BLUE, COLOR_BLACK);
				lcd_update_screen();
				if (t2 - x45 == 1){
					mdelay(1000);
					x45 = gpio_get_value(23);
					if (x45 == 1){
						lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
						lcd_put_str(10, 20, "Press middle button 1 time for continue game", Font_7x10, COLOR_BLACK, COLOR_BLACK);
						lcd_put_str(0, 60, "Press middle button for 1second for exit game", Font_7x10, COLOR_BLACK, COLOR_BLACK);
						lcd_put_str(60, 120, "SCORE: ", Font_16x26, COLOR_BLACK, COLOR_BLACK);
						lcd_put_str(170, 120,  x21, Font_16x26, COLOR_BLACK, COLOR_BLACK);
						lcd_update_screen();
						t = 1;
					}
					else if (x45 == 0){
						break;
					}
				}
				while (t == 1) {

					lcd_set_address_window(pict_x, pict_y, pict_x + PICT_WIDTH - 1, pict_y + PICT_HEIGHT - 1);
					lcd_write_data((u8*)pict_0, sizeof(u16) * PICT_WIDTH * PICT_HEIGHT);
					
					lcd_set_address_window(pict_x1, pict_y1, pict_x1 + PICT_WIDTH1 - 1, pict_y1 + PICT_HEIGHT1 - 1);
					lcd_write_data((u8*)pict_2, sizeof(u16) * PICT_WIDTH1 * PICT_HEIGHT1);
					
					x45 = gpio_get_value(23);
					
					if (x45 == 0){
						t = 0;
						t3 = x45;
					}

					x30 = gpio_get_value(18);
					if (x30 == 0){
						if (pict_y1 + dir_y1 > 0) {
							pict_y1 -= dir_y1;
						}
					}
					x35 = gpio_get_value(24);
					if (x35 == 0){
						if (pict_y1 + PICT_HEIGHT1 + dir_y1 < LCD_HEIGHT) {
							pict_y1 += dir_y1;
						}
					}
					for (x5 = 0; x5 < PICT_WIDTH1; x5++) {
						for (y5 = 0; y5 < PICT_HEIGHT1; y5++) {
							pict_2[y5][x5] = frame_buffer[pict_x1 + x5 + LCD_WIDTH * (pict_y1 + y5)];
							if (pict_mask1[y5][x5]) {
								pict_3[y5][x5] = COLOR_WHITE;
							}
							else {
								pict_3[y5][x5] = pict_2[y5][x5];
							}
						}
					}
					
					if (pict_x + PICT_WIDTH + dir_x > LCD_WIDTH) {
						dir_x *= (-1);
					}
					if (pict_y + PICT_HEIGHT + dir_y > LCD_HEIGHT) {
						dir_y *= (-1);
					}
					for (x10 = 0; x10 < PICT_HEIGHT1; x10++){
						for (x15 = 0; x15 < PICT_HEIGHT; x15++){
							for (x40 = 0; x40 < 5; x40++){
								if (pict_x + dir_x == pict_x1 + x40){
									if (pict_y + x40 + x15 == pict_y1 + x10){
										dir_x *= (-1);
										x10 = PICT_HEIGHT1;
										x15 = PICT_HEIGHT;
										x40 = 5;
									}
								}
							}
						}
					}
					if (pict_x + dir_x < 0) {
						t = 0;
						x20++;
						pict_x = 50, pict_y = 50;
						pict_x1 = 10, pict_y1 = 50;
						if (x500 == 1){
							dir_x = 1, dir_y = 1;
						}
						if (x500 == 2){
							dir_x = 2, dir_y = 2;
						}
						if (x500 == 3){
							dir_x = 3, dir_y = 3;
						}
						if (x500 == 4){
							dir_x = 4, dir_y = 4;
						}
					}
					if (pict_y + dir_y < 0) {
						dir_y *= (-1);
					}
					pict_x += dir_x;
					pict_y += dir_y;
					
					for (x = 0; x < PICT_WIDTH; x++) {
						for (y = 0; y < PICT_HEIGHT; y++) {
							pict_0[y][x] = frame_buffer[pict_x + x + LCD_WIDTH * (pict_y + y)];
							if (pict_mask[y][x]) {
								pict_1[y][x] = COLOR_WHITE;
							}
							else {
								pict_1[y][x] = pict_0[y][x];
							}
						}
					}
					
					lcd_set_address_window(pict_x1, pict_y1, pict_x1 + PICT_WIDTH1 - 1, pict_y1 + PICT_HEIGHT1 - 1);
					lcd_write_data((u8*)pict_3, sizeof(u16) * PICT_WIDTH1 * PICT_HEIGHT1);
					
					
					lcd_set_address_window(pict_x, pict_y, pict_x + PICT_WIDTH - 1, pict_y + PICT_HEIGHT - 1);
					lcd_write_data((u8*)pict_1, sizeof(u16) * PICT_WIDTH * PICT_HEIGHT);
					mdelay(10);
				}
				if (t3 == 0){
					mdelay(1000);
					x45 = gpio_get_value(23);
					if (x45 == 1){
						t3 = 1;
					}
					else if (x45 == 0){
						break;
					}
				}
				t2 = x45;
			}while(t1 == 1);
			//lcd_update_screen();


			memset(buffer, COLOR_BLACK, sizeof(buffer));
			lcd_set_address_window(0, 0, LCD_WIDTH - 1, LCD_HEIGHT - 1);
			x1 = 0;
			y1 = 0;
			w1 = 220;
			h1 = 320;
			color1 = COLOR_COLOR565(0, 0, 0);

			if ((x1 >= LCD_WIDTH) || (y1 >= LCD_HEIGHT)) {
				return 1;
			}

			if ((x1 + w1 - 1) > LCD_WIDTH) {
				w1 = LCD_WIDTH - x1;
			}

			if ((y1 + h1 - 1) > LCD_HEIGHT) {
				h1 = LCD_HEIGHT - y1;
			}

			for (j2 = 0; j2 < h1; j2++) {
				for (i2 = 0; i2 < w1; i2++) {
					buffer[(x1 + LCD_WIDTH * y1) + (i2 + LCD_WIDTH * j2)] = (color1 >> 8) | (color1 << 8);
				}
			}

			lcd_write_data((u8*)buffer, sizeof(u16) * LCD_WIDTH * LCD_HEIGHT);





			return 0;

			out:
			return ret;
		}

		module_init(mod_init);
		module_exit(mod_exit);
