#!/bin/bash

cd /sys/class/gpio
echo 26 > export
echo 16 > export
echo 20 > export
echo 21 > export
cd gpio26
echo in > direction
cd -
cd gpio16
echo out > direction
cd -
cd gpio20
echo out > direction
cd -
cd gpio21
echo out > direction
cd --

