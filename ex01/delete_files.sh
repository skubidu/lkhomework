#!/bin/bash

usage() {
  echo -e "\n"
  echo 'Usage:'
  echo "$0 --dir; help"
}
while [[ $1 = -?* ]]; do
  case $1 in
    -h|--help)
        usage >&2; exit 0
        ;;
    -d|--dir)
        shift; DIR="${1}"
        ;;
    *) echo "invalid option: '$1'."; usage; exit 1; ;;
  esac
  shift
done
args+=("$@")

# Check exists and type of file
check_dir()
{
  if [[ ! -d ${DIR} ]]
  then
    echo "Error: Dir ${DIR} doesn't exists";
    exit 1;
  fi
}

check_dir

# Delete files
del_files()
{
 find ${DIR} \( -iname "*.tmp"  -o -iname "-*" -o -iname "_*" -o -iname "~*" \) -exec rm -r {} \;
}

del_files

